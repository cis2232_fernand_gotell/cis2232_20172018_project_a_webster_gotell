package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Booking;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Integer> {

    List<Booking> findByUserId(int id);
    
}