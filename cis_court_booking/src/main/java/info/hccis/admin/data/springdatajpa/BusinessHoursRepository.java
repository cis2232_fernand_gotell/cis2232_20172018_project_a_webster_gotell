package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.BusinessHours;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessHoursRepository extends CrudRepository<BusinessHours, Integer> {

}