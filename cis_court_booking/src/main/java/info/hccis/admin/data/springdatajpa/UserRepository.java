package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    List<User> findByUsername(String username);
//    User findByName(String username);
}