package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Schedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Integer> {

}