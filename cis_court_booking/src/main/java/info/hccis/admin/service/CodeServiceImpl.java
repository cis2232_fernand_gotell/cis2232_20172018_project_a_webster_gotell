package info.hccis.admin.service;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.dao.CodeTypeDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.model.jpa.CodeType;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Mostly used as a facade for all controllers
 *
 * @author BJ MacLean
 * @since 20151013
 */

@Service
public class CodeServiceImpl implements CodeService {

    private final CodeTypeRepository ctr;
    private final UserRepository ur;
    
    @Autowired
    public CodeServiceImpl(CodeTypeRepository ctr, UserRepository ur){
        this.ctr = ctr;
        this.ur = ur;
    }

    
    
    public CodeTypeRepository getCtr() {
        return ctr;
    }
    
     public  ArrayList<CodeType> getCodeTypes() {
         System.out.println("returning code values...from CodeServiceImpl");
         return CodeTypeDAO.getCodeTypes();
         
     }

    @Override
    public UserRepository getUr() {
        return ur;
    }

    @Override
    public ArrayList<User> getUsers() {
        ArrayList<User> temp = (ArrayList<User>) ur.findAll();
        
        //Also load the user descriptions.  These are stored associated with the 
        //CodeValue code type/sequence of the user type.
        DatabaseConnection dbConnection = new DatabaseConnection();
        for(User current: temp){
            current.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(1, current.getUserTypeCode()));
        }
        return temp;
        
    }

     
     
    
}
