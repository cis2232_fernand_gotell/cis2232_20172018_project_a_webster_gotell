package info.hccis.admin.web;

import info.hccis.admin.dao.BookingDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.CourtDAO;
import info.hccis.admin.dao.ScheduleDAO;
import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.Booking;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.Schedule;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.Utility;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class CourtController {

    private final CourtRepository cr;
    private final BookingRepository br;
    private final UserRepository ur;
    private final CodeValueRepository cvr;
    
    @Autowired
    public CourtController(CourtRepository cr, BookingRepository br, UserRepository ur, CodeValueRepository cvr) {
        this.cr = cr;
        this.br = br;
        this.ur = ur;
        this.cvr = cvr;
    }
    
    // added by fernand, place golder mappings to get to Show pages (bookings and reports)
    @RequestMapping("/court/bookingDetails")
    public String showBookings(Model model, HttpSession session){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList bookings = (ArrayList) br.findByUserId(((User) session.getAttribute("loggedInUser")).getUserId());
        
        model.addAttribute("bookings", bookings);
        return "court/bookingDetails";
    }
    @RequestMapping("/reports/summaryReportDateSelect")
    public String showSummaryReportDate(Model model, HttpSession session){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        return "reports/summaryReportDateSelect";
    }
    @RequestMapping("/reports/summaryReport")
    public String showSummaryReport(Model model, HttpServletRequest request, HttpSession session){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        int days = 0;
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();
        Date startDate = null;
        Date endDate = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try{
        startDate = df.parse(start);
        endDate = df.parse(end);      
        
        cal1.setTime(startDate);
        cal2.setTime(endDate);
        
        days = (int)( (cal2.getTime().getTime() - cal1.getTime().getTime()) / (1000 * 60 * 60 * 24));
        
        } catch (ParseException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ArrayList <Booking> allBookings = (ArrayList) br.findAll();
        ArrayList <Booking> validBookings = new ArrayList<>();
        for (Booking temp : allBookings){
            Date bookingDate = null;
            try{
            bookingDate = df.parse(temp.getBookingDate()); 
            } catch (ParseException ex ){
                
            }
            if (bookingDate.after(startDate) && bookingDate.before(endDate)){
                validBookings.add(temp);
            }
        }
        
        int numSquash = 0;
        int numTennis = 0;
        int numRacquet = 0;
        
        // query to count number of each court type        
        for (Booking check : validBookings){
            if (cr.findOne(check.getCourtId()).getCourtType()== 3){ // Squash
                numSquash++;
            } else if (cr.findOne(check.getCourtId()).getCourtType()== 4){ // Racquet
                numRacquet++;
            } else { // Tennis
                numTennis++;
            }
        }
        
        model.addAttribute("numDays", days);
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("numSquash", numSquash);
        model.addAttribute("numTennis", numTennis);
        model.addAttribute("numRacquet", numRacquet);
                
        return "reports/summaryReport";
    }
    
    @RequestMapping("/court/search")
    public String showSearch(Model model, HttpSession session){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        model.addAttribute("codeValues", CodeValueDAO.getCodeValues("2"));
        model.addAttribute("booking", new Booking());
        model.addAttribute("canBook", BookingDAO.getFutureBookingsCount(((User) session.getAttribute("loggedInUser")).getUserId()) < 2);
        
        return "court/search";
    }
    
    @RequestMapping("/court/selectCourt")
    public String showBooking(@Valid @ModelAttribute("booking") Booking booking, BindingResult result, Model model, HttpSession session, HttpServletRequest request){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        //redirect back to search if accessed manually
        if(booking.getBookingDate() == null){
            System.out.println("Booking object doesn't exist");
            model.addAttribute("codeValues", CodeValueDAO.getCodeValues("2"));
            model.addAttribute("booking", new Booking());
            model.addAttribute("canBook", BookingDAO.getFutureBookingsCount(((User) session.getAttribute("loggedInUser")).getUserId()) < 2);
            return "court/search";
        }
        
        //get the type of court that was selected
        int courtType = Integer.parseInt(request.getParameter("courtType"));
        //check if the user entered a date (only check for date errors)
        if(result.hasFieldErrors("bookingDate")){
            model.addAttribute("codeValues", CodeValueDAO.getCodeValues("2"));
            //used to keep the type of court that was selected when reloading the page
            model.addAttribute("courtTypeSelected", courtType);
            model.addAttribute("booking", booking);
            model.addAttribute("canBook", BookingDAO.getFutureBookingsCount(((User) session.getAttribute("loggedInUser")).getUserId()) < 2);
            return "court/search";
        }
        
        model.addAttribute("courtType", courtType);
        System.out.println("Court Type: " + courtType);
        //get all the courts of that type
        ArrayList<Court> courts = CourtDAO.getCourtsByType(courtType);
        //get the english description for the type of court
        model.addAttribute("courtTypeEng", CodeValueDAO.getCodeValueDescription(2, courtType));
        //add the courts that were found to the model
        model.addAttribute("courts", courts);
        
        //sets the userId of the booking object to the id of the user logged in
        int userId = ((User) session.getAttribute("loggedInUser")).getUserId();
        booking.setUserId(userId);
        ArrayList<ArrayList<Schedule>> courtTimes = new ArrayList();
        //loop through the courts
        for(Court current: courts){
            //add the times for the current court into the 2D arraylist
            courtTimes.add(ScheduleDAO.getAvailableTimes(booking.getBookingDate(), current.getCourtId()));
        }
        model.addAttribute("courtTimes", courtTimes);
        //get all users so the user can choose an opponent
        model.addAttribute("opponents", UserAccessDAO.getOtherUsers(userId));
        Booking test = booking.copy();
        model.addAttribute("booking", test);
       
        return "court/selectCourt";
    }
    
    @RequestMapping("/court/submit")
    public String bookCourt(@Valid @ModelAttribute("booking") Booking booking, BindingResult result, Model model, HttpSession session, HttpServletRequest request){
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        //does a double check if the user already has 2 bookings
        boolean canBook = BookingDAO.getFutureBookingsCount(((User) session.getAttribute("loggedInUser")).getUserId()) < 2;
        if(!canBook){
            model.addAttribute("codeValues", CodeValueDAO.getCodeValues("2"));
            model.addAttribute("booking", new Booking());
            model.addAttribute("canBook", canBook);
            return "court/search";
        }
        
        if(result.hasErrors()){
            int courtType = Integer.parseInt(request.getParameter("courtType"));
            //get all the courts of that type
            ArrayList<Court> courts = CourtDAO.getCourtsByType(courtType);
            //get the english description for the type of court
            model.addAttribute("courtTypeEng", CodeValueDAO.getCodeValueDescription(2, courtType));
            model.addAttribute("courtType", courtType);
            //add the courts that were found to the model
            model.addAttribute("courts", courts);

            //sets the userId of the booking object to the id of the user logged in
            int userId = ((User) session.getAttribute("loggedInUser")).getUserId();
            booking.setUserId(userId);
            ArrayList<ArrayList<Schedule>> courtTimes = new ArrayList();
            //loop through the courts
            for(Court current: courts){
                //add the times for the current court into the 2D arraylist
                courtTimes.add(ScheduleDAO.getAvailableTimes(booking.getBookingDate(), current.getCourtId()));
            }
            model.addAttribute("courtTimes", courtTimes);
            //get all users so the user can choose an opponent
            model.addAttribute("opponents", UserAccessDAO.getOtherUsers(userId));
            model.addAttribute("booking", booking);
            
            return "court/selectCourt";
        }
        
        //check if it is already booked (can only happen if someone books it at the same time)
        if(!BookingDAO.isThisOneBooked(booking.getCourtId(), booking.getBookingDate(), booking.getBookingStartTime())){
            //add other generic information about the booking object
            Date datetime = new Date();
            String id = String.valueOf(booking.getUserId());
            booking.setCreatedDateTime(datetime);
            booking.setCreatedUserId(id);
            booking.setUpdatedDateTime(datetime);
            booking.setUpdatedUserId(id);
            //add booking to the model after using save() so that new auto ID is applied
            model.addAttribute(br.save(booking));
            
            // add booking to audit file
            User user = (User) session.getAttribute("loggedInUser");
            String writeToFile = Utility.getNow() + " " + user.getUsername() + " courtId=" + booking.getCourtId() + " " + booking.getBookingDate() + " " + booking.getBookingStartTime();
            Utility.writeToFile(writeToFile, "/courtbookings/booking.out");
        }else{
            //notify the user that an error occurred
            model.addAttribute("message", "This booking has been booked. Please pick another time.");
            int courtType = Integer.parseInt(request.getParameter("courtType"));
            //get all the courts of that type
            ArrayList<Court> courts = CourtDAO.getCourtsByType(courtType);
            //get the english description for the type of court
            model.addAttribute("courtTypeEng", CodeValueDAO.getCodeValueDescription(2, courtType));
            //add the courts that were found to the model
            model.addAttribute("courts", courts);

            //sets the userId of the booking object to the id of the user logged in
            int userId = ((User) session.getAttribute("loggedInUser")).getUserId();
            booking.setUserId(userId);
            ArrayList<ArrayList<Schedule>> courtTimes = new ArrayList();
            //loop through the courts
            for(Court current: courts){
                //add the times for the current court into the 2D arraylist
                courtTimes.add(ScheduleDAO.getAvailableTimes(booking.getBookingDate(), current.getCourtId()));
            }
            model.addAttribute("courtTimes", courtTimes);
            //get all users so the user can choose an opponent
            model.addAttribute("opponents", UserAccessDAO.getOtherUsers(userId));
            model.addAttribute("booking", booking);
            return "court/selectCourt";
        }
        
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findByUserId(((User) session.getAttribute("loggedInUser")).getUserId());
        model.addAttribute("bookings", bookings);
        
        return "court/bookingDetails";
    }
}
