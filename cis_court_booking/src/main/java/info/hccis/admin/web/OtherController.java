package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.Utility;
import info.hccis.user.bo.UserBO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final UserRepository ur;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr, UserRepository ur) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.ur = ur;
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {

        session.removeAttribute("loggedInUser");
        // add login attempt to audit file
        Date current = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String writeToFile = dateFormat.format(current)+" "+user.getUsername()+" ";
        
        if (!UserBO.authenticate(user, ur)) {
            //failed validation            
            session.removeAttribute("loggedInUser");
            model.addAttribute("message", "Authentication failed");
            writeToFile+= false;
            Utility.writeToFile(writeToFile, "/courtbookings/login.txt");
            return "other/welcome";
        } else {

            //passed and send to the list page (user/list)
            //Get the users from the database
            session.setAttribute("loggedInUser", UserBO.getUserByUsername(user, ur));
            
            //System.out.println("User code = "+UserBO.getUserByUsername(user, ur).getUserTypeCode());
            User loggedIn = UserBO.getUserByUsername(user, ur);

            session.setAttribute("userCode", loggedIn.getUserTypeCode());

            ArrayList<User> users = (ArrayList<User>) ur.findAll();
            System.out.println("JW-found " + users.size() + " users.  Going to user list page");
            model.addAttribute("users", users);
            
            writeToFile+= true;
            Utility.writeToFile(writeToFile, "c:/courtbookings/login.txt");
            return "users/list";
        }
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        // System.out.println("Count from code_type="+ctr.count());

//        ArrayList<CodeType> cts = (ArrayList<CodeType>) ctr.findByEnglishDescription("User Types");
//        System.out.println("BJM-"+cts);
//        
//        ArrayList<CodeType> codeTypesByBob = (ArrayList<CodeType>) ctr.findByCreatedUserId("Bob");
//     
//        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
//        System.out.println("Here are the Code values");     
//        for(CodeValue thisOne: codeValues){
//            System.out.println(thisOne);
//        }
//        System.out.println("Here are the Code types created by bob");     
//        for(CodeType thisOne: codeTypesByBob){
//            System.out.println(thisOne);
//        }
//        
//        //Create a new code value here.
//        CodeValue newCodeValue = new CodeValue(4,1,"TestNew","TN2");
//        cvr.save(newCodeValue);
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }
    
    @RequestMapping("/other/logout")
    public String logout (Model model, @ModelAttribute("user") User user, HttpSession session){
                
        // add logout to audit file
        Date current = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        user = (User) session.getAttribute("loggedInUser");
        String writeToFile = dateFormat.format(current)+" "+user.getUsername()+" logout successful";
        Utility.writeToFile(writeToFile, "c:/courtbookings/logout.txt");
        
        session.removeAttribute("loggedInUser");
        
        return "other/welcome";
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {

        System.out.println("in controller for /");
        //creates a new temporary user object to login
        model.addAttribute("user", new User());
        session.removeAttribute("userCode");

        return "other/welcome";
    }

}
