package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.BusinessHoursRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.ScheduleRepository;
import info.hccis.admin.model.jpa.BusinessHours;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.Schedule;
import info.hccis.admin.model.jpa.User;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Fernand Gotell
 */
@Controller
public class AdminController {

    private final BusinessHoursRepository bhr;
    private final ScheduleRepository sr;
    private final CourtRepository cr;
    private final CodeValueRepository cvr;

    @Autowired
    public AdminController(BusinessHoursRepository bhr, ScheduleRepository sr, CourtRepository cr, CodeValueRepository cvr) {
        this.bhr = bhr;
        this.sr = sr;
        this.cr = cr;
        this.cvr = cvr;
    }

    // court mappings
    @RequestMapping("/admin/adminCourt")
    public String showCourt(Model model, HttpSession session) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList courts = (ArrayList) cr.findAll();

        model.addAttribute("courts", courts);
        return "admin/adminCourt";
    }
    @RequestMapping("/admin/deleteAdminCourt")
    public String deleteCourt(Model model, HttpSession session, HttpServletRequest request) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        cr.delete(Integer.parseInt(request.getParameter("id")));

        ArrayList courts = (ArrayList) cr.findAll();
        model.addAttribute("courts", courts);
        return "admin/adminCourt";
    }
    @RequestMapping("/admin/addCourt")
    public String addCourt(Model model, HttpSession session, @ModelAttribute("court") Court court) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList courtTypes = (ArrayList) cvr.findByCodeTypeId(2);
        ArrayList courts = (ArrayList) cr.findAll();

        model.addAttribute("courtTypes", courtTypes);
        model.addAttribute("courts", courts);
        return "admin/addCourt";
    }
    @RequestMapping("/admin/addSubmitCourt")
    public String addSubmitCourt(Model model, HttpSession session, @ModelAttribute("court") Court court) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();
        court.setCreatedDateTime(new Date());
        court.setUpdatedDateTime(new Date());
        court.setCreatedUserId(role);
        court.setUpdatedUserId(role);

        cr.save(court);

        ArrayList courtTypes = (ArrayList) cvr.findByCodeTypeId(2);
        ArrayList courts = (ArrayList) cr.findAll();
        model.addAttribute("courtTypes", courtTypes);
        model.addAttribute("courts", courts);
        return "admin/adminCourt";
    }
    @RequestMapping("/admin/editAdminCourt")
    public String editCourt(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("court") Court court) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        Court selected = cr.findOne(Integer.parseInt(request.getParameter("id")));
        ArrayList courtTypes = (ArrayList) cvr.findByCodeTypeId(2);
        ArrayList courts = (ArrayList) cr.findAll();

        model.addAttribute("court", selected);
        model.addAttribute("courtTypes", courtTypes);
        model.addAttribute("courts", courts);
        return "admin/editCourt";
    }
    @RequestMapping("/admin/editSubmitCourt")
    public String editSubmitCourt(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("court") Court court) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();   
        court.setCourtId(Integer.parseInt(request.getParameter("courtId")));
        court.setCourtType(Integer.parseInt(request.getParameter("courtType")));
        court.setCreatedDateTime((Date)cr.findOne(court.getCourtId()).getCreatedDateTime());
        court.setCreatedUserId(cr.findOne(court.getCourtId()).getUpdatedUserId());
        
        court.setUpdatedDateTime(new Date());
        court.setUpdatedUserId(role);
        
        cr.save(court);
        
        ArrayList courtTypes = (ArrayList) cvr.findByCodeTypeId(2);
        ArrayList courts = (ArrayList) cr.findAll();
        model.addAttribute("courtTypes", courtTypes);
        model.addAttribute("courts", courts);
        return "admin/adminCourt";
    }

    // schedule mappings
    @RequestMapping("/admin/adminSchedule")
    public String showScheduleAdmin(Model model, HttpSession session) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList schedule = (ArrayList) sr.findAll();
        model.addAttribute("schedule", schedule);
        return "admin/adminSchedule";
    }
    @RequestMapping("/admin/deleteAdminSchedule")
    public String deleteScheduleAdmin(Model model, HttpSession session, HttpServletRequest request) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        sr.delete(Integer.parseInt(request.getParameter("id")));

        ArrayList schedule = (ArrayList) sr.findAll();
        model.addAttribute("schedule", schedule);
        return "admin/adminSchedule";
    }
    @RequestMapping("/admin/addSchedule")
    public String addSchedule(Model model, HttpSession session, @ModelAttribute("schedule") Schedule schedule) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        return "admin/addSchedule";
    }
    @RequestMapping("/admin/addSubmitSchedule")
    public String addSubmitSchedule(Model model, HttpSession session, @ModelAttribute("schedule") Schedule schedule) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();
        schedule.setCreatedDateTime(new Date());
        schedule.setUpdatedDateTime(new Date());
        schedule.setCreatedUserId(role);
        schedule.setUpdatedUserId(role);

        sr.save(schedule);

        ArrayList schedules = (ArrayList) sr.findAll();
        model.addAttribute("schedule", schedules);;
        return "admin/adminSchedule";
    }
    @RequestMapping("/admin/editAdminSchedule")
    public String editSchedule(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("schedule") Schedule schedule) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        Schedule selected = sr.findOne(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("schedule", selected);
;
        return "admin/editSchedule";
    }
    @RequestMapping("/admin/editSubmitSchedule")
    public String editSubmitSchedule(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("schedule") Schedule schedule) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();   
        schedule.setScheduleId(Integer.parseInt(request.getParameter("scheduleId")));
        schedule.setCreatedDateTime((Date)cr.findOne(schedule.getScheduleId()).getCreatedDateTime());
        schedule.setCreatedUserId(sr.findOne(schedule.getScheduleId()).getCreatedUserId());
        
        schedule.setUpdatedDateTime(new Date());
        schedule.setUpdatedUserId(role);
        
        schedule.setStartTime(request.getParameter("startTime"));
        schedule.setEndTime(request.getParameter("endTime"));
       
        sr.save(schedule);
        
        ArrayList schedules = (ArrayList) sr.findAll();
        model.addAttribute("schedule", schedules);
        return "admin/adminSchedule";
    }
    
    // business hours mappings
    @RequestMapping("/admin/adminHours")
    public String showHoursAdmin(Model model, HttpSession session) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList businessHours = (ArrayList) bhr.findAll();
        model.addAttribute("hours", businessHours);
        return "admin/adminHours";
    }
    @RequestMapping("/admin/deleteAdminHours")
    public String deleteHoursAdmin(Model model, HttpSession session, HttpServletRequest request) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        bhr.delete(Integer.parseInt(request.getParameter("id")));

        ArrayList hours = (ArrayList) bhr.findAll();
        model.addAttribute("hours", hours);
        return "admin/adminHours";
    }
    @RequestMapping("/admin/addHours")
    public String addHours(Model model, HttpSession session, @ModelAttribute("hours") BusinessHours hours) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        return "admin/addHours";
    }
    @RequestMapping("/admin/addSubmitHours")
    public String addSubmitHours(Model model, HttpSession session, @ModelAttribute("hours") BusinessHours hours) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();
        hours.setCreatedDateTime(new Date());
        hours.setUpdatedDateTime(new Date());
        hours.setCreatedUserId(role);
        hours.setUpdatedUserId(role);

        bhr.save(hours);

        ArrayList theHours = (ArrayList) bhr.findAll();
        model.addAttribute("hours", theHours);

        return "admin/adminHours";
    }
    @RequestMapping("/admin/editAdminHours")
    public String editHours(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("hours") BusinessHours hours) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        BusinessHours selected = bhr.findOne(Integer.parseInt(request.getParameter("id")));
        
        model.addAttribute("hours", selected);        
        return "admin/editHours";
    }
    @RequestMapping("/admin/editSubmitHours")
    public String editSubmitHours(Model model, HttpSession session, HttpServletRequest request, @ModelAttribute("hours") BusinessHours hours) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        User temp = (User) session.getAttribute("loggedInUser");
        int userType = temp.getUserTypeCode();
        String role = cvr.findOne(userType).getEnglishDescription();   
        hours.setBusinessHoursId(Integer.parseInt(request.getParameter("businessHoursId")));
        hours.setDayType(Integer.parseInt(request.getParameter("dayType")));
        hours.setCreatedDateTime((Date)cr.findOne(hours.getBusinessHoursId()).getCreatedDateTime());
        hours.setCreatedUserId(cr.findOne(hours.getBusinessHoursId()).getUpdatedUserId());
        
        hours.setStartTime(request.getParameter("startTime"));
        hours.setEndTime(request.getParameter("endTime"));
        
        hours.setUpdatedDateTime(new Date());
        hours.setUpdatedUserId(role);
        
        bhr.save(hours);
        
        ArrayList businessHours = (ArrayList) bhr.findAll();
        model.addAttribute("hours", businessHours);
        return "admin/adminHours";
    }
}