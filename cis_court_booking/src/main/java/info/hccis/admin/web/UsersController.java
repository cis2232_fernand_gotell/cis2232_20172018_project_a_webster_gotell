package info.hccis.admin.web;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.service.CodeService;
import info.hccis.admin.util.Utility;
import info.hccis.admin.web.services.UserAccess;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class UsersController {

    private final CodeService codeService;
    private final CodeValueRepository cvr;

//    public UsersController(){
//        //default constructor for injection.
//    }
    @Autowired
    public UsersController(CodeService cs, CodeValueRepository cvr) {
        this.codeService = cs;
        this.cvr = cvr;
    }

    @RequestMapping("/users/list")
    public String showUsers(Model model, HttpSession session) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        ArrayList<User> users = UserAccessDAO.getUsers();
        model.addAttribute("users", users);
        
        System.out.println("this is working");
        return "users/list";
    }

    @RequestMapping("/users/add")
    public String addUser(Model model, HttpSession session, @ModelAttribute("user") User user) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
        UserAccess userAccess = new UserAccess();
        
        model.addAttribute("userTypes", cvr.findByCodeTypeId(1));
//replacing with jpa        
        String result = "pass";
        if (user != null && user.getUsername() != null && !user.getUsername().isEmpty()) {
            //Setting Hashed password
            user.setPassword(userAccess.getHashPassword(user.getPassword()));
            if(user.getUserId() == null){
                user.setUserId(0);
                user.setCreatedDateTime(Utility.getNow(""));
            }
            try {
                codeService.getUr().save(user);
                
            } catch (Exception e) {
                result = "fail";
                System.out.println("There was an error saving the user.");
                e.printStackTrace();
            }
            if (!result.equals("Fail")) {
                model.addAttribute("message", "User added");
                model.addAttribute("user", new User());

                //Load users into the session for drop down.
                ArrayList<User> users = codeService.getUsers();
                
                for(User current: users){
                       current.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(1, current.getUserTypeCode()));
                }
                session.setAttribute("users", users);

                model.addAttribute("users", users);
                return "users/list";
            } else {
                model.addAttribute("message", "Failed to add user");
                model.addAttribute("user", user);
            }
        }

        return "users/add";

    }

//    @RequestMapping("/users/edit")
//    public String editUser(Model model, HttpSession session, HttpServletRequest request){
//        UserAccess getUser = new UserAccess();
//        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        System.out.println(request.getParameter("id"));
//        User selectUser = getUser.selectUser(Integer.parseInt(request.getParameter("id")), databaseConnection);
//        System.out.println(selectUser.getUserAccessId());
//        model.addAttribute("user", selectUser);
//        return "users/edit";
//    }
//    @RequestMapping("/users/edit")
//    public String edit(Model model, HttpSession session, HttpServletRequest request){
//        UserAccess edit = new UserAccess();
//        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        User editUser = new User();
//        editUser.setUserAccessId(Integer.parseInt(request.getParameter("id")));
//        editUser.setUsername(request.getParameter("username"));
//        editUser.setPassword(request.getParameter("password"));
//        editUser.setUserTypeCode(Integer.parseInt(request.getParameter("userTypeCode")));
//        
//        edit.updateUser(editUser, databaseConnection);
//        return "users/list";
//    }
    @RequestMapping("/users/delete")
    public String deleteUser(Model model, HttpSession session, HttpServletRequest request) {
        //if the user is not logged in, redirect them
        if(session.getAttribute("loggedInUser") == null){
            System.out.println("in controller for /");
            //creates a new temporary user object to login
            model.addAttribute("user", new User());
            session.removeAttribute("userCode");
            return "other/welcome";
        }
        
//        UserAccess deleteUser = new UserAccess();
//        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        deleteUser.removeUser(Integer.parseInt(request.getParameter("id")), databaseConnection);
//        ArrayList<User> users = UserAccessDAO.getUsers(databaseConnection);
//        model.addAttribute("users", users);

        codeService.getUr().delete(Integer.parseInt(request.getParameter("id")));
        //Load users into the session for drop down.
        ArrayList<User> users = codeService.getUsers();
        session.setAttribute("users", users);
        model.addAttribute("users", users);
        return "users/list";
    }

}
