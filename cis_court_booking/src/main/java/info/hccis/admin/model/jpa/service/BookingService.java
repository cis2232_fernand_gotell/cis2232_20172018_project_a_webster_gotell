package info.hccis.admin.model.jpa.service;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.model.jpa.Booking;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/BookingService")
public class BookingService {

    @Resource
    private final BookingRepository br;

    public BookingService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BookingRepository.class);
    }

    @GET
    @Path("/api")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();
        
        services.add("/bookings/{userId}");
        
        Gson gson = new Gson();
        String temp = "";
        temp = gson.toJson(services);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    
    @GET
    @Path("/bookings/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBookings(@PathParam("userId") int userId) {
        //Get the bookings from the database
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAll();
        ArrayList<Booking> searchResults = new ArrayList<>();
        // sample date string 12/15/2017        
        Date today = new Date();
        SimpleDateFormat dFormat = new SimpleDateFormat("mm/dd/yyyy");
        String dateCheck = dFormat.format(today);

        System.out.println(dateCheck);
        
        for (Booking current : bookings) {
            if (current.getBookingDate().compareTo(dateCheck) <= 0) {
                if (current.getUserId() == userId) {
                    searchResults.add(current);
                }
            }
        }

        int statusCode = 200;
        if (bookings.isEmpty()) {
            statusCode = 204;
        }

        Gson gson = new Gson();
        String temp = "";
        temp = gson.toJson(searchResults);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    @GET
    @Path("/bookings/{userId}/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBookings(@PathParam("userId") int userId, @PathParam("startDate") String startDate, @PathParam("endDate") String endDate) {
        //Get the booking from the database
        // sample date string 12/15/2017        
        startDate = startDate.replaceAll("-", "/");
        endDate = endDate.replaceAll("-", "/");

        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAll();
        ArrayList<Booking> searchResults = new ArrayList<>();
        for (Booking current : bookings) {

            if (current.getUserId() == userId) {
                if (current.getBookingDate().compareTo(startDate) >= 0) {
                    if (current.getBookingDate().compareTo(endDate) <= 0) {
                        searchResults.add(current);
                    }
                }
            }
        }

        Gson gson = new Gson();

        String temp = "";
        temp = gson.toJson(searchResults);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
