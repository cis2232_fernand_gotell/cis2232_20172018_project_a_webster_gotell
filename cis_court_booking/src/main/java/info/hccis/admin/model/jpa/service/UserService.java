package info.hccis.admin.model.jpa.service;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.User;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author Fernand Gotell
 * @since Jan 26, 2018
 *
 */
@Path("/UserService")
public class UserService {

    @GET
    @Path("/api")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        services.add("/user/userName/hashedPassword");
        services.add("example --> /user/bmaclean/202cb962ac59075b964b07152d234b70");
        services.add("/user");
        services.add("example --> /user");

        Gson gson = new Gson();
        String temp = "";
        temp = gson.toJson(services);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    @Resource
    private final UserRepository ur;

    public UserService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.ur = applicationContext.getBean(UserRepository.class);
    }

    @GET
    @Path("/user/{username}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("username") String user, @PathParam("password") String password) {

//        User login = ur.findOne(1);
        ArrayList<User> login = (ArrayList<User>) ur.findByUsername(user);
//        User login = ur.findByName(user);

        Gson gson = new Gson();
        //convert the user to JSON
        String temp;
        //check if any errors occurred
        int statusCode = 200;
        if (login.isEmpty() || !(login.get(0).getPassword().equalsIgnoreCase(password))) {
            //if the logins array is empty, or the password doesn't match, return a new null user
            temp = gson.toJson(new User());
        } else {
            //if the passwords match, then return the 
            // testing match
            System.out.println("DEBUG user password " + password);
            System.out.println("DEBUG db password " + login.get(0).getPassword());
            temp = gson.toJson(login.get(0));
        }

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    /**
     * This method will get all users and return as a json array
     *
     * @return users as json
     * @since 20180206
     * @author BJM
     */
    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        ArrayList<User> users = (ArrayList<User>) ur.findAll();

        Gson gson = new Gson();
        //convert the user to JSON
        String temp;
        //check if any errors occurred
        int statusCode = 200;

        temp = gson.toJson(users);
        

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

}
