package info.hccis.admin.model.jpa.service;

import com.google.gson.Gson;
import info.hccis.admin.dao.ScheduleDAO;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.Schedule;
import info.hccis.admin.util.Utility;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import org.json.JSONException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/CourtService")
public class CourtService {

    @Resource
    private final CourtRepository cr;

    public CourtService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CourtRepository.class);
    }

    @GET
    @Path("/api")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        services.add("/courts/courtType/startDateInclusive/endDateExclusive");
        services.add("example --> /courts/3/01-24-2018/01-25-2018");

        Gson gson = new Gson();
        String temp = "";
        temp = gson.toJson(services);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    
    @GET
    @Path("/courts/{courtType}/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCourts(@PathParam("courtType") int courtType, @PathParam("startDate") String startDate, @PathParam("endDate") String endDate) {
        //dates are entered with hyphens for easier input ('/' results to '&F'), then replace them afterwards      
        startDate = startDate.replaceAll("-", "/");
        endDate = endDate.replaceAll("-", "/");
        
        //get all of the courts of this type
        ArrayList<Court> courts = (ArrayList<Court>) cr.findByCourtType(courtType);
        //create list of hashmaps (linkedHashMaps are used instead of JSON objects to maintain the order in which values are added)
        ArrayList<LinkedHashMap> availableCourts = new ArrayList();
        
        //used to loop through days
        LocalDate localStartDate;
        LocalDate localEndDate;
        LocalDate localCurrentDate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        try {
            //convert start and end date to LocalDate object
            localStartDate = LocalDate.parse(startDate, formatter);
            localEndDate = LocalDate.parse(endDate, formatter);
            localCurrentDate = LocalDate.parse(Utility.getNow("MM/dd/yyyy"), formatter);

            try{
                //loop through each court
                for(Court court : courts){
                    //loop through the days from start date to end date
                    for (LocalDate date = localStartDate; date.isBefore(localEndDate); date = date.plusDays(1)){
                        //checks if the start date passed in is not in the past (if the start date is in the past, it will only record from the current date)
                        //if both dates are in the past, empty array, if start date in the past and end in the future, record from current date to end
                        //if start date is after end date, empty array
                        if(date.isEqual(localCurrentDate) || date.isAfter(localCurrentDate)){
                            //create hashmap, keys will represent attributes
                            LinkedHashMap linkedMap = new LinkedHashMap();
                            //add the courtId, courtType, and current date in the loop to the hashmap
                            linkedMap.put("courtId", court.getCourtId());
                            linkedMap.put("courtType", court.getCourtType());
                            linkedMap.put("date", formatter.format(date));
                            //get the available times for this court on this date
                            ArrayList<Schedule> availableTimes = ScheduleDAO.getAvailableTimes(formatter.format(date), court.getCourtId());
                            //store the start and end times in separate arrays
                            String[] startTimes = new String[availableTimes.size()];
                            String[] endTimes = new String[availableTimes.size()];
                            //loop through schedule object and assign values to each array
                            for(int i = 0; i < availableTimes.size(); i++){
                                startTimes[i] = availableTimes.get(i).getStartTime();
                                endTimes[i] = availableTimes.get(i).getEndTime();
                            }
                            //add the start and end times to the hashmap
                            linkedMap.put("startTimes", startTimes);
                            linkedMap.put("endTimes", endTimes);

                            //add the hashmap to the arraylist
                            availableCourts.add(linkedMap);
                            
                        }
                    }
                }
            } catch (JSONException jsonEx) {
                jsonEx.printStackTrace();
            }
        }
        catch (DateTimeParseException dtpe) {
            dtpe.printStackTrace();
        } 

        //check if any errors occurred
        int statusCode = 200;
        if (availableCourts.isEmpty()) {
            statusCode = 204;
        }
        
        Gson gson = new Gson();

        //convert the arraylist of hashmaps to JSON
        String temp = gson.toJson(availableCourts);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
