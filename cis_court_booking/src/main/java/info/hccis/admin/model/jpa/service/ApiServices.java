package info.hccis.admin.model.jpa.service;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/api")
public class ApiServices {

    public ApiServices(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        String output = "Services available\r\n\r\n";
        output += "__________________________BookingService_________________________\r\n";
        output += "Get bookings by userid\r\n";
        output += "           /bookings/{userId}\r\n";
        output += "example:   http://hccis.info:8080/courtbooking/rest/BookingService/bookings/1\r\n\r\n";
        output += "__________________________CourtService_________________________\r\n";
        output += "Get availabe courts between two dates (not end date exlusive)\r\n";
        output += "           /courts/courtType/startDateInclusive/endDateExclusive\r\n";
        output += "example    http://hccis.info:8080/courtbooking/rest/CourtService/courts/3/01-24-2018/01-25-2018\r\n\r\n";
        output += "__________________________UserService_________________________\r\n";
        output += "Login user (returns user object as json)\r\n";
        output += "           /user/userName/hashedPassword\r\n";
        output += "example    http://hccis.info:8080/courtbooking/rest/UserService/user/bmaclean/202cb962ac59075b964b07152d234b70\r\n\r\n";
        output += "Get all users";
        output += "           /user\r\n";
        output += "example    http://hccis.info:8080/courtbooking/rest/UserService/user\r\n";

        return Response.status(200).entity(output).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
