package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Booking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Purpose: this class adds the CodeType object to the database
 *
 * @return
 * @author Jordan Campbell
 * @since 20151002
 */
public class BookingDAO {

    /**
     * Get all of the code types.
     *
     * @return List of CodeType's
     * @since 20151015
     * @author BJ MacLean
     */
    public static boolean isThisOneBooked(int courtId, String bookingDate, String bookingStartTime) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        boolean isBooked = true;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `Booking` WHERE `courtId` = " + courtId + " AND `bookingDate` = '" + bookingDate + "' AND `bookingStartTime` = '" + bookingStartTime + "'";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            isBooked = rs.next();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return isBooked;

    }

    public static int getFutureBookingsCount(int userId) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        int numBookings = -1;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM Booking WHERE userId = " + userId + " order by bookingDate";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            int count = 0;
            while (rs.next()) {

                //Get the booking date in yyyymmdd format
                Date startDate = null;
                Date now = new Date();
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                startDate = df.parse(rs.getString("bookingDate"));
                if (now.compareTo(startDate) > 0) {
                    count++;
                }

            }
            numBookings = count;

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return numBookings;
    }
}
