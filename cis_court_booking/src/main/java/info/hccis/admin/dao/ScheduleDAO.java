package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Schedule;
import info.hccis.admin.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Purpose: this class adds the CodeType object to the database
 *
 * @return
 * @author Jordan Campbell
 * @since 20151002
 */
public class ScheduleDAO {

    /**
     * Get all of the times in the schedule
     *
     * @return List of CodeType's
     * @since 20151015
     * @author BJ MacLean
     */
    public static ArrayList<Schedule> getSchedule() {
        ArrayList<Schedule> slots = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT startTime, endTime FROM Schedule";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                Schedule slot = new Schedule();
                slot.setStartTime(rs.getString("startTime"));
                slot.setEndTime(rs.getString("endTime"));
                System.out.println("Found schedule =" + slot.getStartTime() + "-" + slot.getEndTime());
                slots.add(slot);
            }
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return slots;

    }

    /**
     * Get all of the times in the schedule
     *
     * @return List of CodeType's
     * @since 20151015
     * @author BJ MacLean
     */
    public static ArrayList<Schedule> getAvailableTimes(String date, int courtId) {
        ArrayList<Schedule> slots = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT startTime, endTime FROM Schedule WHERE startTime NOT IN "
                    + "(SELECT bookingStartTime FROM Booking WHERE bookingDate = '" + date + "' AND courtId = " + courtId + ") AND "
                    + "startTime >= (SELECT startTime FROM BusinessHours WHERE dayType = "
                    + "(SELECT DAYOFWEEK(STR_TO_DATE('" + date + "','%m/%d/%Y')))) AND "
                    + "endtime <= (SELECT endTime FROM BusinessHours WHERE dayType = "
                    + "(SELECT DAYOFWEEK(STR_TO_DATE('" + date + "','%m/%d/%Y'))))";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                Schedule slot = new Schedule();
                
                //check if the date is today
                if(date.equals(Utility.getNow("MM/dd/yyyy"))){
                    //only include times after the current time
                    int currentTime = Integer.parseInt(Utility.getNow("HHmm"));
                    if(currentTime < Integer.parseInt(rs.getString("startTime"))){
                        slot.setStartTime(rs.getString("startTime"));
                        slot.setEndTime(rs.getString("endTime"));
                        slots.add(slot);
                    }
                }else{
                    //use times normally if it is not today
                    slot.setStartTime(rs.getString("startTime"));
                    slot.setEndTime(rs.getString("endTime"));
                    slots.add(slot);
                }
                
                System.out.println("Found schedule =" + slot.getStartTime() + "-" + slot.getEndTime());
                
            }
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return slots;

    }
}
