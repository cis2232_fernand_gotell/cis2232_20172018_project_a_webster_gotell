package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Court;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Purpose: this class adds the CodeType object to the database
 *
 * @return
 * @author Jordan Campbell
 * @since 20151002
 */
public class CourtDAO {

    /**
     * Get all of the code types.
     *
     * @return List of CodeType's
     * @since 20151015
     * @author BJ MacLean
     */
    public static ArrayList<Court> getCourtsByType(int courtType) {
        ArrayList<Court> courts = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `Court` WHERE `courtType` = " + courtType;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                Court court = new Court();
                court.setCourtId(rs.getInt("courtId"));
                court.setCourtType(rs.getInt("courtType"));
                System.out.println("Found court =" + court);
                courts.add(court);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courts;

    }
    
    public static int getCourtType(int courtId){
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        int courtType = -1;
        
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT DISTINCT `courtType` FROM `Court` WHERE `courtId` = " + courtId;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                courtType = rs.getInt("courtType");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return courtType;
    }
}
