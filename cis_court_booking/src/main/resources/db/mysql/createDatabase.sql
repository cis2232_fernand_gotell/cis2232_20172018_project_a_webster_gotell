--
-- Database: cis2232_court_booking
--
DROP DATABASE cis2232_court_booking;
create database cis2232_court_booking;
use cis2232_court_booking;

-- --------------------------------------------------------
grant select, insert, update, delete on cis2232_court_booking.*
             to 'cis2232_admin'@'localhost'
             identified by 'Test1234';
flush privileges;


--
-- Table structure for table CodeType
--

CREATE TABLE CodeType (
  codeTypeId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Table structure for table CodeValue
--

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Table structure for table User
--

CREATE TABLE User (
  userId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  lastName varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  additional1 varchar(100),
  additional2 varchar(100),
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When user was created.'
);


INSERT INTO CodeType (codeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 'Court Types', 'User Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'Admin', 'Admin', 'AdminFR', 'AdminFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(1, 2, 'General', 'General', 'GeneralFR', 'GeneralFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 3, 'Squash', 'Squash', 'SquashFR', 'SquashFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 4, 'Racketball', 'Racketball', 'Racketball', 'Racketball', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(2, 5, 'Tennis', 'Tennis', 'Tennis', 'Tennis', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO User (userId, username, password, lastName, firstName, userTypeCode, additional1, additional2, createdDateTime) VALUES
(1, 'bmaclean', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 1, NULL, NULL, '2017-12-06 21:23:21'),
(2, 'neon', '202cb962ac59075b964b07152d234b70', 'Gotell', 'Fernand', 2, NULL, NULL, '2017-12-07 17:55:12'),
(3, 'test', '202cb962ac59075b964b07152d234b70', 'Test', 'Test', 2, NULL, NULL, '2017-12-07 17:55:25');

INSERT INTO `booking` (`bookingId`, `userId`, `courtId`, `opponent`, `bookingDate`, `bookingStartTime`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 1, NULL, '12/08/2017', '0603', '2017-12-07 17:03:43', '0', '2017-12-07 17:03:43', '0'),
(2, 1, 1, NULL, '12/15/2017', '0603', '2017-12-07 17:03:53', '0', '2017-12-07 17:03:53', '0'),
(3, 1, 1, NULL, '12/22/2017', '0603', '2017-12-07 17:04:01', '0', '2017-12-07 17:04:01', '0'),
(4, 2, 1, NULL, '12/10/2017', '0720', '2017-12-07 17:54:02', '0', '2017-12-07 17:54:02', '0'),
(5, 2, 6, NULL, '12/17/2017', '0720', '2017-12-07 17:54:14', '0', '2017-12-07 17:54:14', '0'),
(6, 2, 5, NULL, '12/24/2017', '0720', '2017-12-07 17:54:26', '0', '2017-12-07 17:54:26', '0'),
(7, 3, 5, NULL, '12/13/2017', '0800', '2017-12-07 17:54:39', '0', '2017-12-07 17:54:39', '0'),
(8, 3, 1, NULL, '12/20/2017', '0800', '2017-12-07 17:54:46', '0', '2017-12-07 17:54:46', '0'),
(9, 3, 1, NULL, '12/27/2017', '0800', '2017-12-07 17:54:57', '0', '2017-12-07 17:54:57', '0');

  
CREATE TABLE Booking (
  bookingId int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  userId int(3) NOT NULL,
  courtId int(3) NOT NULL,
  opponent int(3),
  bookingDate varchar(20),
  bookingStartTime varchar(20),
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
);
  
CREATE TABLE Schedule (
  scheduleId int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  startTime varchar(20),
  endTime varchar(20),
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
);

CREATE TABLE Court (
  courtId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  courtType int(3) not null COMMENT 'Code Type=2',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
);

CREATE TABLE BusinessHours (
  businessHoursId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dayType int(3) not null COMMENT 'Code Type=2',
  startTime varchar(20),
  endTime varchar(20),
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
);

delete from Court;
INSERT INTO Court(courtId, courtType, createdDateTime, createdUserId, updatedDateTime,updatedUserId) 
VALUES 
(1, 3, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(2, 3, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(3, 3, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(4, 3, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(5, 4, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(6, 5, sysdate(),'ADMIN',sysdate(),'ADMIN'),
(7, 5, sysdate(),'ADMIN',sysdate(),'ADMIN');


delete from BusinessHours;
INSERT INTO BusinessHours(businessHoursId, dayType, startTime, endTime, createdDateTime, createdUserId, updatedDateTime, updatedUserId) 
VALUES 
(1,1,'0800','1900',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(2,2,'0600','2130',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(3,3,'0600','2130',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(4,4,'0600','2130',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(5,5,'0600','2130',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(6,6,'0600','2130',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(7,7,'0800','1900',sysdate(),'ADMIN',sysdate(),'ADMIN');

delete from Schedule;
INSERT INTO Schedule(scheduleId, startTime, endTime, createdDateTime, createdUserId, updatedDateTime, updatedUserId)
VALUES 
(1,'0600','0640',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(2,'0640','0720',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(3,'0720','0800',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(4,'0800','0840',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(5,'0840','0920',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(6,'0920','1000',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(7,'1000','1040',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(8,'1040','1120',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(9,'1120','1200',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(10,'1200','1240',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(11,'1240','1320',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(12,'1320','1400',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(13,'1400','1440',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(14,'1440','1520',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(15,'1520','1600',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(16,'1600','1640',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(17,'1640','1720',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(18,'1720','1800',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(19,'1800','1840',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(20,'1840','1920',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(21,'1920','2000',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(22,'2000','2040',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(23,'2040','2120',sysdate(),'ADMIN',sysdate(),'ADMIN'),
(24,'2120','2200',sysdate(),'ADMIN',sysdate(),'ADMIN');