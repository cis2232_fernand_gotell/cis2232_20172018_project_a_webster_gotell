# README #

Court booking final project, owned and maintained by Jordan Webster and Fernand Gotell.
Write priveledges granted to BJ Maclean, to facilitate instructor role over final project.

Remaining fields to be completed as project is completed.

README updated by Fernand Gotell on Oct 11, 2017
README updated by Fernand Gotell on Dec 9 2017

### What is this repository for? ###

* Quick summary
* Release Version 1.0

### How do I get set up? ###

* To get started, open project and perform the following tasks:
- right-click the package at the top of the project windo in netbeans, and build with dependancies
- open Other Sources -> src/main/resources -> db.mysql , and right click createDatabase.sql. choose Run File, making sure to select your local host connection
- click the green arrow in the top tray, or press F6 to start to project
- after a few moments, you may see a Tomcat login popop
- use user "admin" and password "admin" to continue (based on your local machine, this may change)
- after a few moments, the default browser will open with the project running

* May need to open either info.hccis.admin.model.jpa.service/CourtService.java or BookingService.java , and right click-> add import for "import com.google.gson.Gson;"
This import seems to need to be updated after being pulled from the repository, but your machine may not need.

### To Begin ###
* After running the createDabasse script, there are 3 available user to sign in as
admin user 	bmaclean
general user	neon
general user	test

All users have same password *123*
All login attempts, successful or not, are logged in a txt file.

In the navigation menu, you have 5 options:
- Menu
- Logout
- Admin (only if signed in as an Admin user)
- Help
- About

### Menu ###
* The main menu gives 3 options:
Users - List of all current users stored in the db. Allows adding new users and deleting current ones.
Book a Court - Allows user to create a court booking. Blocks adding new booking if current user already has 2 or more future bookings.
Bookings - List of all bookings for currently logged in user.

### Logout ### 
* Removes current user from browser session and returns to sign in page. Logouts are also logged in a txt file.

### Admin (for Admin ranked users only ###
* List contains 4 menu options:
- Court Admin
- Schedule Admin
- Hours Admin
- Summary Report

All "Admin" pages provide CRUD funtionality to the user, for the respective database table.
Allows user to Create, Read, Update, or Delete data.

- Summary Report
Allows admin user to view counts of court types booked in a selected timeframe. User selectes a start and end date using date pickers, then procedes to the report.

### Help ###
* Not a requirement

### About ###
* Not a requirement

### Audit System ###
After first login attmept, a directory named "courtbookings" will be created in the c: drive.
This directory will house all audit logs.

### Web Services ###
Once the main program is running, you can view the available web services by opening a new tab/browser and loading one of the following urls

- http://localhost:8080/courtbooking/rest/CourtService/courts/{courtType}/{startDate}/{endDate}
- http://localhost:8080/courtbooking/rest/BookingService/bookings/{userId}/{startDate}/{endDate}
- http://localhost:8080/courtbooking/rest/BookingService/bookings/{userId}
(booking service use the following date format MM-DD-YYYY)

Samples for BookingService
- http://localhost:8080/courtbooking/rest/BookingService/bookings/0/12-16-2017/12-30-2017
- http://localhost:8080/courtbooking/rest/BookingService/bookings/1

### Video Samples Submissions ###
* Sample videos of program in use can be found at the following links

https://gaming.youtube.com/watch?v=Hzc_ZFARS_c (Part 2)